<?php
require_once("utils.php");


if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["password2"]) && isset($_POST["realname"])) {
  if($_POST["password"] === $_POST["password2"]) {
    $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'www');

    $password = sha1($_POST["password"] . SALT);

    $stmt = $db->prepare("INSERT INTO user(username, password_hash, realname) VALUES(:f1, :f2, :f3)");

    $stmt->execute(array(":f1" => $_POST["username"], ":f2" => $password, ":f3" => $_POST["realname"]));

    //$stmt->debugDumpParams();

    print "<p>Käyttäjätili luotu.</p>";

  }
  else {
    print "<p>Salasanat eivät täsmää!</p>";

  }

}
else {
print <<<REGISTERFORM

<form action="index.php?p=register" method="post">
<input type="text" name="username" placeholder="Käyttäjänimi" />
<input type="password" name="password" placeholder="Salasana" />
<input type="password" name="password2" placeholder="Salasana uudestaan" />
<input type="text" name="realname" placeholder="Kokonimi" />
<input type="submit" value="Rekisteröidy" />

</form>
REGISTERFORM;
}
