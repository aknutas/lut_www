<?php

$db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'www');

$stmt = $db->prepare("SELECT id, realname FROM user");
$stmt->execute();

$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

print(json_encode($rows));

?>
