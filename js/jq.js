$(document).ready(function(){

  $("input, textarea").focusin(function(){
    $(this).css("font-size", 24);
  });
  $("input, textarea").focusout(function(){
    $(this).css("font-size", 12);
  });

  $("#menuButton").click(function(event) {
    $("nav ul").toggle("slow");


  });

  $("main *").click(function(event) {
    $(this).remove();
  });


});
