<?php
require_once("utils.php");
?>


<script>
function loadUsers() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("users").innerHTML = xhttp.responseText;
    }
  }
  xhttp.open("GET", "users.php", true);
  xhttp.send();
}

function loadUsersjQuery() {
  $.getJSON("users.php", function(data) {
    var text="<ul>";
    data.forEach(function(entry) {
      text += "<li>" + entry.id + ": " + entry.realname + "</li>";
    });
    text += "</ul>"
    document.getElementById("users2").innerHTML = text;
  });
}

$(document).ready(function() {
  $('#addUser').click(function() {
    var username = $("#username").val();
    var realname = $("#realname").val();
    var addButton = $("#addUser");

    addButton.attr("disabled", "disabled");
    addButton.text("Lisätään...");

    var request = $.ajax({
      url: "register.php",
      type: "POST",
      data: {
        "username": username,
        "password": "salasana",
        "password2": "salasana",
        "realname": realname

      },
      dataType: "html"
    });

    request.done(function(msg){
      alert(msg);
      addButton.removeAttr("disabled");
      addButton.text("Lisää käyttäjä");
    });

    request.fail(function(jqXHR, textStatus) {
      alert(textStatus);
      addButton.removeAttr("disabled");
      addButton.text("Lisää käyttäjä");
    });


  });
});




</script>

<div onclick="loadUsers();">Lataa käyttäjät</div>
<div onclick="loadUsersjQuery();">Lataa käyttäjät jQueryllä</div>
<input name="username" id="username" placeholder="Username" />
<input name="realname" id="realname" placeholder="Real name" />
<input type="button" id="addUser" value="Lisää käyttäjä" />


<br />

<div id="users"></div>
<div id="users2"></div>
